﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class zEditor
{
  [MenuItem("zNox/Build/Android", false, 10)]
  protected static void BuildAndroid()
  {
    BuildPipeline.BuildPlayer(zAppConfig.Scene, string.Format("./Builds/Android/{0}.apk", zConfig.applicationName), BuildTarget.Android, BuildOptions.None);
  }

  [MenuItem("zNox/Build/IOS", false, 10)]
  protected static void BuildIOS()
  {
    BuildPipeline.BuildPlayer(zAppConfig.Scene, string.Format("./Builds/IOS/{0}", zConfig.applicationName), BuildTarget.iOS, BuildOptions.None);
  }

  [MenuItem("zNox/Build/WebGL", false, 10)]
  protected static void BuildWebGL()
  {
    BuildPipeline.BuildPlayer(zAppConfig.Scene, string.Format("./Builds/WebGL/{0}", zConfig.applicationName), BuildTarget.WebGL, BuildOptions.None);
  }

  [MenuItem("zNox/Build/BuildAll", false, 10)]
  protected static void BuildAll()
  {
    BuildPipeline.BuildPlayer(zAppConfig.Scene, string.Format("./Builds/Android/{0}.apk", zConfig.applicationName), BuildTarget.Android, BuildOptions.None);
    BuildPipeline.BuildPlayer(zAppConfig.Scene, string.Format("./Builds/IOS/{0}",         zConfig.applicationName), BuildTarget.iOS, BuildOptions.None);
    BuildPipeline.BuildPlayer(zAppConfig.Scene, string.Format("./Builds/WebGL/{0}",       zConfig.applicationName), BuildTarget.WebGL, BuildOptions.None);
  }

  [MenuItem("zNox/Update Config", false, 100)]
  protected static void UpdateConfig()
  {
    string path = Application.dataPath + "/zLibs/zConfig.cs";

    using (System.IO.StreamWriter sw = System.IO.File.CreateText(path))
    {
      string str = string.Empty;

      str += "public static class zConfig" + Environment.NewLine;
      str += "{" + Environment.NewLine;

      foreach (KeyValuePair<string, string> property in zAppConfig.StringValue)
      {
        str += string.Format("  public static readonly string {0} = \"{1}\";", property.Key, property.Value) + Environment.NewLine;
      }

      foreach (KeyValuePair<string, int> property in zAppConfig.IntValue)
      {
        str += string.Format("  public static readonly int {0} = {1};", property.Key, property.Value) + Environment.NewLine;
      }

      str += "}";

      sw.WriteLine(str);
    }
  }

  [MenuItem("zNox/Update PlayerSettings", false, 100)]
  protected static void UpdatePlayerSetting()
  {
    //DefineSymbols
    PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, "");

    foreach (KeyValuePair<string, bool> plugIn in zAppConfig.Plugins)
    {
      if (plugIn.Value) PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, "zNox_" + plugIn.Key + ";");
      if (plugIn.Value) PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS, "zNox_" + plugIn.Key + ";");
    }

    //PlayerSettings
    PlayerSettings.companyName = "pyrice";
    PlayerSettings.productName = "Donuts Combo: Origins";
    PlayerSettings.bundleVersion = zConfig.gameVersion;
    PlayerSettings.Android.bundleVersionCode = zConfig.versionCode;
  }
}
#endif
