public static class zConfig
{
  public static readonly string androidPackageName = "com.pyrice.donuts";
  public static readonly string applicationName = "Donuts Combo: Origins";
  public static readonly string gameVersion = "1.0.0";
  public static readonly string interstitialID = "";
  public static readonly string bannerID = "";
  public static readonly string rewardVideoID = "ca-app-pub-5794510790148939/1093137003";
  public static readonly int versionCode = 1;
}
