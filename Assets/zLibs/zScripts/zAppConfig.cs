﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public static class zAppConfig
{
  public static readonly Dictionary<string, bool> Plugins = new Dictionary<string, bool>()
  {
    { "Ads", true }
  };


  public static readonly Dictionary<string, string> StringValue = new Dictionary<string, string>()
  {
    { "androidPackageName"      , "com.pyrice.donuts" },
    { "applicationName"         , "Donuts Combo: Origins" },
    { "gameVersion"             , "1.0.0" },
    { "interstitialID"          , "" },
    { "bannerID"                , "" },
    { "rewardVideoID"           , "ca-app-pub-5794510790148939/1093137003" }
  };

  public static readonly Dictionary<string, int> IntValue = new Dictionary<string, int>()
  {
    { "versionCode", 1 }
  };

  public static readonly string[] Scene =
  {
    "Assets/Scenes/Init.unity",
    "Assets/Scenes/Login.unity",
    "Assets/Scenes/Loading.unity",
    "Assets/Scenes/Lobby.unity",
    "Assets/Scenes/Game.unity"
  };
}
#endif