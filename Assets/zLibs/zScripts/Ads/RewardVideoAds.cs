﻿#if zNox_Ads
using GoogleMobileAds.Api;
using System;
using UnityEngine;

public static class RewardVideoAds
{
  private static string ADS_ID = zConfig.rewardVideoID;

  private static RewardBasedVideoAd _Instance = RewardBasedVideoAd.Instance;

  private static Action _OnComplete = null;

  public static void Setup()
  {
    _Instance.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
    _Instance.OnAdRewarded += HandleRewardBasedVideoRewarded;
    _Instance.OnAdClosed += HandleRewardBasedVideoClosed;

    Request();
  }

  public static void Request()
  {
    AdRequest adRequest = new AdRequest.Builder().Build();

    _Instance.LoadAd(adRequest, ADS_ID);
  }

  public static void Show(Action on_complete = null)
  {
    if (_Instance.IsLoaded())
    {
      _OnComplete = on_complete;
      _Instance.Show();
    }
  }

  public static void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
  {
    Request();
  }

  public static void HandleRewardBasedVideoClosed(object sender, EventArgs args)
  {
    _OnComplete = null;
    Request();
  }

  public static void HandleRewardBasedVideoRewarded(object sender, Reward args)
  {
    _OnComplete?.Invoke();
    _OnComplete = null;
  }
}
#endif