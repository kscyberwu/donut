﻿#if zNox_Ads
using GoogleMobileAds.Api;

public static class BannerAds
{
  private static string ADS_ID = zConfig.bannerID;

  private static BannerView bannerAd = new BannerView(ADS_ID, AdSize.SmartBanner, AdPosition.Top);
  public static void Request()
  {
    AdRequest adRequest = new AdRequest.Builder().Build();

    bannerAd.LoadAd(adRequest);
  }
  public static void Show()
  {
    bannerAd.Show();
  }
}
#endif