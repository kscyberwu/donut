﻿#if zNox_Ads
using GoogleMobileAds.Api;

public class InterstitialAds
{
  private static string ADS_ID = zConfig.interstitialID;

  private static InterstitialAd _Instance = new InterstitialAd(ADS_ID);

  public static void Request()
  {
    AdRequest adRequest = new AdRequest.Builder().Build();

    _Instance.LoadAd(adRequest);
  }

  public static void Show()
  {
    if (_Instance.IsLoaded())
    {
      _Instance.Show();
    }
  }
}
#endif