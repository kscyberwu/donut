﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingScene : MonoBehaviour
{
  private static LoadingScene _Instance = null;
  public static LoadingScene GetInstance() { return _Instance; }

  [SerializeField] private Image _Progress = default;

  protected void Awake()
  {
    _Instance = this;
  }

  public void UpdateProgress(float progress)
  {
    _Progress.fillAmount = progress;
  }
}
