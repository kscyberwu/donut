﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
  private static Game _Instance = null;
  public static Game GetInstance() { return _Instance; }

  private static PlayerData _PlayerData = null;
  public static PlayerData GetPlayerData() { return _PlayerData; }

  [SerializeField] private AbilityData _AbilityData = default;
  public AbilityData GetAbilityData() { return _AbilityData; }
  public float GetAbilityValue(Ability.Type type, int level)
  {
    foreach (Ability ab in _AbilityData.data)
    {
      if (ab.type == type)
      {
        return ab.info[level].value;
      }
    }
    return 0;
  }

  protected void Awake()
  {
    if (_Instance)
    {
      Destroy(gameObject);
      return;
    }

    DontDestroyOnLoad(gameObject);
    _Instance = this;
    _PlayerData = new PlayerData();
    PlayerEvent.UpdateCoin();
  }

  public static void UpdateCoin(int coin)
  {
    _PlayerData.coin += coin;
    _PlayerData.Save();
    PlayerEvent.UpdateCoin();
  }
  public void LoadScene(string sceneName)
  {
    GetComponent<SceneLoader>().Load(sceneName);
  }
  public void ChangeBGM(string name)
  {
    SoundManager sm = GetComponent<SoundManager>();
    sm.PlayBGM(sm.GetAudioClip(name));
  }
  public void PlayOneShotSound(string name)
  {
    SoundManager sm = GetComponent<SoundManager>();
    sm.PlayOneShot(sm.GetAudioClip(name));
  }
  public void PlayOneShotSound(AudioClip clip)
  {
    SoundManager sm = GetComponent<SoundManager>();
    sm.PlayOneShot(clip);
  }
}
