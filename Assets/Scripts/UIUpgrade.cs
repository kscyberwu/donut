﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIUpgrade : MonoBehaviour
{
  [SerializeField] private GameObject _UpgradeObject = default;
  [SerializeField] private Transform _Content = default;

  protected void Awake()
  {
    UpdateObject();
  }

  public void UpdateObject()
  {
    StartCoroutine(DelayUpdateObject());
  }

  private IEnumerator DelayUpdateObject()
  {
    Clear();

    yield return new WaitForEndOfFrame();

    foreach (Ability data in Game.GetInstance().GetAbilityData().data)
    {
      GameObject obj = Instantiate(_UpgradeObject, _Content, false);
      obj.GetComponent<UpgradeObject>().Setup(data);
    }
  }

  private void Clear()
  {
    foreach (Transform t in _Content)
    {
      Destroy(t.gameObject);
    }
  }



}
