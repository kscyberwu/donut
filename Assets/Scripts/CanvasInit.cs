﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasInit : MonoBehaviour
{
  protected void Start()
  {
    SetupAds();
    Application.targetFrameRate = 60;
    Game.GetInstance().LoadScene("Login");
  }

  private void SetupAds()
  {
#if zNox_Ads
    RewardVideoAds.Setup();
#endif
  }
}
