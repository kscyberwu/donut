﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerEvent
{
  public delegate void UpdateCoinEvent();
  public static event UpdateCoinEvent OnUpdateCoin;

  public static void UpdateCoin()
  {
    OnUpdateCoin?.Invoke();
  }
}
