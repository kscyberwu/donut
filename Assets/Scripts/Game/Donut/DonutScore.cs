﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DonutScore : DonutBase
{
  [SerializeField] private int score;

  protected override void DonutClickEffect()
  {
    DonutGame.GetInstance().UpdateScore(50);
    DonutGame.GetInstance().UpdateCombo();
  }

  protected override void DonutDestroyEffect()
  {
    DonutGame.GetInstance().ResetCombo(true);
  }
}
