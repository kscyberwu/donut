﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class DonutBase : MonoBehaviour
{
  [SerializeField] private RectTransform _Rect = default;
  [SerializeField] private Rigidbody2D _Rigidbody2D = default;
  [SerializeField] private GameObject _EffectClick = default;
  [SerializeField] private float _DestroyEffect = default;

  private bool reflected = false;
  private bool destroying = false;

  protected abstract void DonutClickEffect();
  protected abstract void DonutDestroyEffect();

  protected void Update()
  {
    CheckReflect();
    CheckDestroy();
  }

  protected void OnMouseDown()
  {
    if (destroying) return;

    destroying = true;
    ShowEffectClick();
    Destroy(gameObject);
    DonutClickEffect();
  }

  private void CheckReflect()
  {
    if (Mathf.Abs(_Rect.anchoredPosition.x) > 1920 / 2 && !reflected)
    {
      reflected = true;
      Vector2 new_force = _Rigidbody2D.velocity;
      new_force.x = -new_force.x * 0.6f;

      _Rigidbody2D.velocity = new_force;
      _Rigidbody2D.angularVelocity = -_Rigidbody2D.angularVelocity;
    }
  }

  private void CheckDestroy()
  {
    if (_Rect.anchoredPosition.y < -200)
    {
      Destroy(gameObject);
      DonutDestroyEffect();
    }
  }

  private void ShowEffectClick()
  {
    if (!_EffectClick) return;
    GameObject obj = Instantiate(_EffectClick, _Rect.parent, false);
    obj.GetComponent<RectTransform>().anchoredPosition = _Rect.anchoredPosition;
    Destroy(obj, _DestroyEffect);
    
  }
}
