﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DonutOver : DonutBase
{
  [SerializeField] private int energyDown = 0;

  protected override void DonutClickEffect()
  {
    DonutGame.GetInstance().UpdateEnergy(-energyDown);
  }

  protected override void DonutDestroyEffect()
  {
  }
}
