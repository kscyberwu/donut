﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
  private const float MAX_SPAWN_DELAY = 1;
  private const float MIN_SPAWN_DELAY = 0.3f;
  private const float SPAWN_DELAY_PER_LEVEL = 0.05f;


  [SerializeField] private GameObject[] _DonutScore = default;
  [SerializeField] private GameObject _DonutOver = default;
  [SerializeField] private uint donutOverPercentage = 20;

  private IEnumerator SpawnCoroutine;
  private GameObject GenerateDonut()
  {
    GameObject donut;

    float percentage = Random.Range(0, 100);

    if (percentage < donutOverPercentage)
    {
      donut = Instantiate(_DonutOver, transform, false);
    }
    else
    {
      int idx = Random.Range(0, _DonutScore.Length);
      donut = Instantiate(_DonutScore[idx], transform, false);
    }

    return donut;
  }
  public void SpawnDonut()
  {
    float force_x = Random.Range(-300, 300);
    float force_y = Random.Range(500, 700);
    float torque = -force_x / 5;

    GameObject obj = GenerateDonut();
    obj.GetComponent<Rigidbody2D>().AddForce(new Vector2(force_x, force_y));
    obj.GetComponent<Rigidbody2D>().AddTorque(torque);
  }
  public void StartSpawn()
  {
    SpawnCoroutine = Spawn();
    StartCoroutine(SpawnCoroutine);
  }
  public void StopSpawn()
  {
    StopCoroutine(SpawnCoroutine);
    ClearDonut();
  }
  public IEnumerator Spawn()
  {
    while (true)
    {
      float SpawnDelay = MAX_SPAWN_DELAY - (DonutGame.GetInstance().GetComboLevel() * SPAWN_DELAY_PER_LEVEL);
      if (SpawnDelay < MIN_SPAWN_DELAY) SpawnDelay = MIN_SPAWN_DELAY;
      yield return new WaitForSeconds(SpawnDelay);
      SpawnDonut();
    }
  }
  private void ClearDonut()
  {
    foreach (Transform t in transform)
    {
      Destroy(t.gameObject);
    }
  }
}
