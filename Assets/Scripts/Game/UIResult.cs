﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIResult : MonoBehaviour
{
  [SerializeField] private Text _Score = default;
  [SerializeField] private Text _MaxCombo = default;
  [SerializeField] private Text _Coin = default;
  [SerializeField] private Button _AdsBTN = default;

  private DonutGameData _Data = null;

  public void ShowResult(DonutGameData data)
  {
    _Data = data;
    UpdateResult();
    gameObject.SetActive(true);
  }

  public void ResultX3()
  {
#if zNox_Ads
    RewardVideoAds.Show(() =>
    {
      _AdsBTN.gameObject.SetActive(false);
      _Data.score *= 3;
      UpdateResult();
    });
#endif
  }
  public void EnterLobby()
  {
    int coin = (int)(_Data.score * DonutGame.COIN_RATE);
    Game.UpdateCoin(coin);
    Game.GetInstance().LoadScene("Lobby");
  }
  private void UpdateResult()
  {
    int coin = (int)(_Data.score * DonutGame.COIN_RATE);

    _Score.text     = ":    " + _Data.score.ToString("n0");
    _MaxCombo.text  = ":    " + _Data.comboMax.ToString("n0");
    _Coin.text      = ":    " + coin.ToString("n0");
  }
}
