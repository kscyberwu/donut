﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DonutGameData
{
  public int score;
  public int comboMax;
  public float energy;
  public float energyMax;
  public bool revived;
  public bool ending;
}

public class DonutGame : MonoBehaviour
{
  [SerializeField] private Spawner _Spawner = default;
  [SerializeField] private Text _Score = default;
  [SerializeField] private Image _ComboProgress = default;
  [SerializeField] private Text _ComboText = default;
  [SerializeField] private Image _EnergyProgress = default;
  [SerializeField] private GameObject _ReviveDialog = default;
  [SerializeField] private UIResult _ResultDialog = default;
  [SerializeField] private AudioClip[] _ComboSounds = default;
  [SerializeField] private AudioClip _NextComboSounds = default;
  [SerializeField] private AudioClip _LostComboSounds = default;

  private static DonutGame _Instance = null;
  public static DonutGame GetInstance() { return _Instance; }

  private DonutGameData _DonutGameData = new DonutGameData() { ending = true };
  private const int DEFAULT_ENERGY = 40;
  private const int MAX_COMBO_POINT = 100;
  private const int COMBO_POINT_PER_CLICK = 10;
  public const float COIN_RATE = 0.01f;
  public const float DECREASE_COMBO_PER_SEC = 2;

  private int _ComboLevel = 1;
  public int GetComboLevel() { return _ComboLevel; }
  private float _ComboPoint = 0;
  private int _ComboSoundIndex = 0;
  private float combo_timer = 1;

  private IEnumerator _ComboCoroutine = null;

  protected void Awake()
  {
    if (_Instance)
    {
      Destroy(gameObject);
      return;
    }

    _Instance = this;

    Game.GetInstance().ChangeBGM("bgm_game");
    NewGame();
  }
  protected void Update()
  {
    UpdateEnergyTime();
    if(!_DonutGameData.ending) UpdateComboPerSec();
  }

  public void UpdateComboPerSec()
  {
    float before = _ComboPoint;
    if (combo_timer < 0)
    {
      combo_timer = 1;
      if (_ComboPoint < 0)
      {
        if (_ComboLevel > 1)
        {
          _ComboPoint = MAX_COMBO_POINT + _ComboPoint;
          _ComboLevel--;
          PlayUpdateProgress(before, _ComboPoint);
        }
        else
        {
          _ComboPoint = 0;
        }
      }
    }
    else
    {
      combo_timer -= Time.deltaTime;
      _ComboPoint -= DECREASE_COMBO_PER_SEC * Time.deltaTime;
      _ComboProgress.fillAmount -= DECREASE_COMBO_PER_SEC * Time.deltaTime / MAX_COMBO_POINT;
      _ComboText.text = "x" + _ComboLevel;
    }
  }
  public void UpdateScore(int score)
  {
    int level = Game.GetPlayerData().GetAbilityLevel(Ability.Type.Score);
    float bonus = 1 + Game.GetInstance().GetAbilityValue(Ability.Type.Score, level);
    _DonutGameData.score += (int)(score * _ComboLevel * bonus);
    UpdateScore();
  }
  public void UpdateEnergy(float energy)
  {
    _DonutGameData.energy += energy;
  }
  public void ResetCombo(bool play_sound = false)
  {
    if (play_sound) Game.GetInstance().PlayOneShotSound(_LostComboSounds);
    float before = _ComboPoint;

    _ComboLevel = 1;
    _ComboPoint = 0;
    _ComboSoundIndex = 0;

    PlayUpdateProgress(before, _ComboPoint);
  }
  public void UpdateCombo()
  {
    float before = _ComboPoint;

    int level = Game.GetPlayerData().GetAbilityLevel(Ability.Type.Combo);
    float bonus = 1 + Game.GetInstance().GetAbilityValue(Ability.Type.Combo, level);
    _ComboPoint += COMBO_POINT_PER_CLICK * bonus;

    if (_ComboPoint < MAX_COMBO_POINT)
    {
      Game.GetInstance().PlayOneShotSound(_ComboSounds[_ComboSoundIndex]);
      if (_ComboSoundIndex < _ComboSounds.Length - 1) _ComboSoundIndex++;
    }
    else
    {
      while (_ComboPoint >= MAX_COMBO_POINT)
      {
        _ComboPoint = _ComboPoint - MAX_COMBO_POINT;
        _ComboLevel++;
        if (_DonutGameData.comboMax < _ComboLevel) _DonutGameData.comboMax = _ComboLevel;
      }
      _ComboSoundIndex = 0;
      Game.GetInstance().PlayOneShotSound(_NextComboSounds);
    }

    PlayUpdateProgress(before, _ComboPoint);
  }
  public void ReviveAccept()
  {
#if zNox_Ads
    RewardVideoAds.Show(() =>
    {
      _ReviveDialog.SetActive(false);
      _DonutGameData.energy = _DonutGameData.energyMax;
      _DonutGameData.ending = false;
      _DonutGameData.revived = true;
      _Spawner.StartSpawn();
    });
#endif
  }
  public void ReviveCancel()
  {
    _ReviveDialog.SetActive(false);
    _DonutGameData.revived = true;
    EndGame();
  }
  private void PlayUpdateProgress(float before, float after)
  {
    if (_ComboCoroutine != null) StopCoroutine(_ComboCoroutine);

    _ComboCoroutine = UpdateProgress(before, after);
    StartCoroutine(_ComboCoroutine);
  }
  private IEnumerator UpdateProgress(float before, float  after)
  {
    _ComboText.text = "x" + _ComboLevel;

    float currentProgress = before;

    if (currentProgress < after)
    {
      while (currentProgress < after)
      {
        yield return new WaitForEndOfFrame();
        currentProgress += 250 * Time.deltaTime;
        _ComboProgress.fillAmount = currentProgress / MAX_COMBO_POINT;
      }
    }
    else
    {
      while (currentProgress > after)
      {
        yield return new WaitForEndOfFrame();
        currentProgress -= 250 * currentProgress * Time.deltaTime;
        _ComboProgress.fillAmount = currentProgress / MAX_COMBO_POINT;
      }
    }

    _ComboProgress.fillAmount = after / MAX_COMBO_POINT;
  }
  private void UpdateEnergyTime()
  {
    if (_DonutGameData.energy > 0)
    {
      _DonutGameData.energy -= Time.deltaTime;
      _EnergyProgress.fillAmount = _DonutGameData.energy / _DonutGameData.energyMax;
    }
    else if (!_DonutGameData.ending)
    {
      _DonutGameData.energy = 0;
      _EnergyProgress.fillAmount = 0;
      TimeOut();
    }
  }
  private void TimeOut()
  {
    EndGame();
  }
  private void NewGame()
  {
    int level = Game.GetPlayerData().GetAbilityLevel(Ability.Type.HP);
    float bonusHP = 1 + Game.GetInstance().GetAbilityValue(Ability.Type.HP, level);

    _DonutGameData = new DonutGameData()
    {
      score     = 0,
      energy    = DEFAULT_ENERGY * bonusHP,
      energyMax = DEFAULT_ENERGY * bonusHP,
      revived   = false,
      ending    = false
    };

    _Spawner.StartSpawn();
    ResetCombo();
  }
  private void UpdateScore()
  {
    _Score.text = _DonutGameData.score.ToString("0");
  }
  private void EndGame()
  {
    _DonutGameData.ending = true;
    _Spawner.StopSpawn();

    if (!_DonutGameData.revived)
    {
      _ReviveDialog.SetActive(true);
    }
    else
    {
      _ResultDialog.ShowResult(_DonutGameData);
    }
  }
}
