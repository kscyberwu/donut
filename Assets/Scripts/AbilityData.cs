﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct AbilityInfo
{
  public string description;
  public float value;
  public int price;
}

[System.Serializable]
public struct Ability
{
  public enum Type
  {
    HP,
    Combo,
    Score
  }

  public string name;
  public Sprite icon;
  public Type type;
  public AbilityInfo[] info;
}

[CreateAssetMenu(fileName = "AbilityData", menuName = "AbilityData", order = 1)]
public class AbilityData : ScriptableObject
{
  public List<Ability> data = new List<Ability>();
}
