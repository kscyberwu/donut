﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCoin : MonoBehaviour
{
  protected void OnEnable()
  {
    PlayerEvent.OnUpdateCoin += UpdateCoin;
    UpdateCoin();
  }

  protected void OnDisable()
  {
    PlayerEvent.OnUpdateCoin -= UpdateCoin;
  }

  private void UpdateCoin()
  {
    GetComponent<Text>().text = Game.GetPlayerData().coin.ToString("n0");
  }
}
