﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeObject : MonoBehaviour
{
  [SerializeField] private Image _Icon = default;
  [SerializeField] private Text _Name = default;
  [SerializeField] private Text _Description = default;
  [SerializeField] private Text _Level = default;
  [SerializeField] private Text _Cost = default;

  private Ability _Data;
  int currentLevel = 0;

  public void Setup(Ability data)
  {
    currentLevel = Game.GetPlayerData().GetAbilityLevel(data.type) + 1;
    _Data = data;
    UpdateObject();
  }

  public void Buy()
  {
    if (currentLevel < _Data.info.Length)
    {
      if (Game.GetPlayerData().coin < _Data.info[currentLevel].price) return;
      Game.UpdateCoin(-_Data.info[currentLevel].price);
      Game.GetPlayerData().SetAbilityLevel(_Data.type, currentLevel);
      currentLevel++;
      UpdateObject();
    }
  }

  private void UpdateObject()
  {
    name                = _Data.name;
    _Name.text          = _Data.name;
    _Icon.sprite        = _Data.icon;
    _Description.text   = currentLevel < _Data.info.Length ? _Data.info[currentLevel].description : "Max";
    _Level.text         = currentLevel < _Data.info.Length ? string.Format("LV : {0}", currentLevel) : "LV : Max";
    _Cost.text          = currentLevel < _Data.info.Length ? string.Format(_Data.info[currentLevel].price.ToString("n0")) : "Max";
  }
}
