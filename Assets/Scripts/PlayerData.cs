﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData
{
  public int coin;
  private Dictionary<Ability.Type, int> abilityLevel = new Dictionary<Ability.Type, int>();

  public int GetAbilityLevel(Ability.Type type)
  {
    int value = 0;
    if (abilityLevel.TryGetValue(type, out value))
    {
      return value;
    }

    return 0;
  }

  public void SetAbilityLevel(Ability.Type type, int level)
  {
    abilityLevel[type] = level;
    Save();
  }

  public void Save()
  {
    PlayerPrefs.SetInt("coin",  coin);
    PlayerPrefs.SetInt("hp",    abilityLevel[Ability.Type.HP]);
    PlayerPrefs.SetInt("combo", abilityLevel[Ability.Type.Combo]);
    PlayerPrefs.SetInt("score", abilityLevel[Ability.Type.Score]);
  }

  public PlayerData()
  {
    coin                              = PlayerPrefs.GetInt("coin");
    abilityLevel[Ability.Type.HP]     = PlayerPrefs.GetInt("hp");
    abilityLevel[Ability.Type.Combo]  = PlayerPrefs.GetInt("combo");
    abilityLevel[Ability.Type.Score]  = PlayerPrefs.GetInt("score");
  }
}
