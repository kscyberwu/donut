﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
  public void Load(string sceneName)
  {
    StartCoroutine(LoadAsynchronously(sceneName));
  }

  private IEnumerator LoadAsynchronously(string sceneName)
  {
    AsyncOperation loading = SceneManager.LoadSceneAsync("Loading");

    while (!loading.isDone)
    {
      float progress = Mathf.Clamp01(loading.progress / 0.9f);

      yield return new WaitForSeconds(0.1f);
    }
 
    AsyncOperation scene_progress = SceneManager.LoadSceneAsync(sceneName);

    while (!loading.isDone)
    {
      float progress = Mathf.Clamp01(scene_progress.progress / 0.9f);

      if (LoadingScene.GetInstance()) LoadingScene.GetInstance().UpdateProgress(progress);

      yield return new WaitForSeconds(0.1f);
    }
  }
}
