﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundBTN : MonoBehaviour
{
  public void PlayOneShotSound(string name)
  {
    Game.GetInstance().PlayOneShotSound(name);
  }
}
