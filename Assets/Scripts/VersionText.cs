﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VersionText : MonoBehaviour
{
  protected void Awake()
  {
    GetComponent<Text>().text = Application.version;
  }
}
