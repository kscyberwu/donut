﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
  private float VolumnBGM = 0.5f;
  private float VolumnSFX = 0.7f;
  private AudioSource _AudioSource = default;

  protected void Awake()
  {
    _AudioSource = GetComponent<AudioSource>();
  }

  public AudioClip GetAudioClip(string clip_name)
  {
    AudioClip clip = Resources.Load<AudioClip>("Sounds/" + clip_name);
    return clip;
  }

  public void PlayBGM(AudioClip clip)
  {
    if (_AudioSource.clip == clip) return;
    _AudioSource.volume = VolumnBGM;
    _AudioSource.clip = clip;
    _AudioSource.Play();
  }

  public void PlayOneShot(AudioClip clip)
  {
    GameObject g = new GameObject("one_shot_sound");
    AudioSource audioSource = g.AddComponent<AudioSource>();
    DontDestroyOnLoad(g);
    audioSource.volume = VolumnSFX;
    audioSource.clip = clip;
    audioSource.Play();
    Destroy(g, clip.length);
  }
}
