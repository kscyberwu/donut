﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasLogin : MonoBehaviour
{
  protected void Awake()
  {
    Game.GetInstance().ChangeBGM("bgm_default");
  }

  public void StartBTN()
  {
    Game.GetInstance().LoadScene("Lobby");
  }
}
