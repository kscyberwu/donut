﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasLobby : MonoBehaviour
{
  [SerializeField] private GameObject _UIUpgrade = default;

  protected void Awake()
  {
    Game.GetInstance().ChangeBGM("bgm_default");
  }

  public void StartBTN()
  {
    Game.GetInstance().LoadScene("Game");
  }

  public void UpgradeBTN()
  {
    _UIUpgrade.SetActive(!_UIUpgrade.activeInHierarchy);
  }
}
